function [outputSignal,gain]=CompresorQueenMary( signal, fs, Treshold, Ratio, attackTime, releaseTime)

  attackCoeff = exp(-1000/(attackTime*44100));
  releaseCoeff = exp(-1000/(releaseTime*44100));

  i=1;
  lastOutput=0;
  n=max(size(signal));
  while (i<n)
    logImput= 20*log10(abs(signal(1,i))/0.7746);
       
    approachGain = 0;
    if (logImput>Treshold)
      approachGain= ( logImput - Treshold )*(Ratio-1);
    end
     gain(i,1)=approachGain;
     if (lastOutput>approachGain)%%Cambiar por logImput
       lastOutput= attackCoeff*lastOutput+((1-attackCoeff)*approachGain);
     else
       lastOutput=releaseCoeff*lastOutput+((1-releaseCoeff)*approachGain);
    end
    
    amplification=(10^(lastOutput/20));
    gain(i,2)=amplification;
    outputSignal(1,i)=signal(1,i)*amplification;
    logOutput= 20*log10(abs(outputSignal(1,i))/0.7746);
    
    i=i+1;

  end
end