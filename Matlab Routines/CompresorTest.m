function UsoDelCompresor(Mode)

  FS=44100;

  ModulacionA=[ones(1,2000)*0.3 ones(1,5000)*2 ones(1,5000)*6 ...
    ones(1,6000)*3 ones(1,4000) ones(1,5000)*1.4 ones(1,4000) ...
    ones(1,2000)*0.1 ones(1,8100)*3 ones(1,3001)];

  Senial=sin(0:2*pi*350/FS:2*pi*350);
  size(ModulacionA)
  size(Senial)
  Senial= Senial.*ModulacionA;

  Treshold= -5;
  Ratio = 0.5;
  Ta=10;
  Tr=10;
  figure(1);
  
  
  plot(Senial);
  set(gca, 'ylim', [-8 8], 'ytick',[ -10^(15/20) -10^(10/20) -10^(5/20) ...
      10^(5/20) 10^(10/20) 10^(15/20)], 'yticklabel', ...
      {'15' '10' '5' '5' '10' '15'});
  ylabel('dB');
  set(gca, 'xlim',[0 44100], 'xtick',[0:0.1*44100:44100], 'xticklabel',...
      {'0' '0.1' '0.2' '0.3' '0.4' '0.5' '0.6' '0.7' '0.8' '0.9' '1'});
  xlabel('Segundos');
  
  CASES= 4;
  figure(2)
  switch(Mode)
    
    case 'Treshold'
        
        Treshold = [ 6 0 -6 -15 ];
        
    case 'Ratio'
        
        Ratio = [ 0.75 0.5 0.25 0.05 ];
        
    case 'Time'
        
        Ta = [ 5 10 50 100 ];
        Tr = [ 5 10 50 100 ];
        
    case 'Audio'
        
        [Senial,FS] = audioread('Audios\Compresor\looperman-l-1395601-0080429-tubthumper-tt39-funky-16th-notes-on-hi-hat-with-swagger.wav');
        Senial=Senial';
        Treshold = [ -25 -10 -10 -10];
        Ratio = [ 0.5 0.1 0.5 0.5 ];
        Ta = [ 20 20 50 20];
        Ta = [ 20 20 20 50];
        

  end
  i=0;
  while( i < CASES )
    [Salida,Ganancia]=CompresorQueenMary( Senial, FS, Treshold(mod(i, ...
        max(size(Treshold)))+1), Ratio(mod(i,max(size(Ratio)))+1), Ta(mod(i, ...
        max(size(Ta)))+1), Tr(mod(i,max(size(Tr)))+1) );
    switch(Mode)
        
      case 'Audio'
        
        [file,path]=uiputfile('*.wav');
        
        
        audiowrite(strcat(path,file),Salida,FS);
          
      otherwise
            
        size(Salida)
        hold off;
        subplot(2,2,i+1);

        plot(Senial,'r');
        hold on;
        subplot(2,2,i+1);
        plot(Salida,'g');
        
        
        set(gca, 'ylim', [-8 8], 'ytick',[ -10^(15/20) -10^(10/20) -10^(5/20) ...
        -1 1 10^(5/20) 10^(10/20) 10^(15/20)], 'yticklabel', ...
        {'15' '10' '5' '0' '0' '5' '10' '15'});
        ylabel('dB');
        set(gca, 'xlim',[0 FS], 'xtick',[0:0.1*44100:44100], 'xticklabel',...
        {'0' '0.1' '0.2' '0.3' '0.4' '0.5' '0.6' '0.7' '0.8' '0.9' '1'});
        xlabel('Segundos');
    end
    i=i+1
  end
end
