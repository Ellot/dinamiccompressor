/*
  This code has been based on:
    -Digital Dynamic Range Compressor Design � A Tutorial and Analysis
     DIMITRIOS GIANNOULIS, MICHAEL MASSBERG, AND JOSHUA D. REISS, AES Member
     Queen Mary University of London, London, UK
*/


#include "m_pd.h"

static t_class *Dinamica_class;

typedef struct _Dinamica
{
  t_object x;

  // Object' auxiliar values
  t_int sSR;  // Signal Sample Rate
  t_float sIn;  // Signal Inlet value
  t_outlet *p_sOut;  // Outlet Handler
  
  // Compression parameters
  t_float Threshold;
  t_float Ratio;
  t_float AttacTime;
  t_float AttacCoeff;
  t_float ReleaseTime;
  t_float p_ReleaseCoeff;
  
  // Auxiliar global proccessing values
  t_float lastOutputGain;

} t_Dinamica;


/* Time Coeff Calculation
   
     Coeff = e^(-t/SR)
*/
float timeCoefficient( float time, int sr )
{

  return exp(  -1000/(float)( time * (float)sr ) );

}


//Level Detection Algorithm using an envelope follower
float levelDetector(float imput_dB,float approachedGain, float lastOutput_dB,
                    float AttacCoeff, float p_ReleaseCoeff)
{
 
  if(lastOutput_dB>approachedGain)
  {

    return (float)(AttacCoeff*lastOutput_dB +
           (1-AttacCoeff)*approachedGain);

  }
  else
  {

    return (float)(p_ReleaseCoeff*lastOutput_dB + 
           (1-p_ReleaseCoeff)*approachedGain);

  }
  
}

/* Stright Gain Calculation

    FinalValue = (dBu_In - TH)*Ratio + TH
    Gain = FinalValue - dBu_In

    Gain = ( dBu_In - TH )*( Ratio - 1 )
*/
float gainComputer(float imput_dB,float Threshold_dB, float Ratio)
{
  
  return((imput_dB-Threshold_dB)*((Ratio)-1));

}

// Linear To Logarithmic
/* It turns lineal value into a logarithmic dbu value using:
   dBu_value = 20*log10(SQRT(lin_value^2)/0.7746)
*/
float linToLog(float imput)
{
  
  return( 20*log10(sqrt(pow(imput,2)/0.7746)));

}

// Logarithmic to linear
/* It turns lineal value into a logarithmic dbu value using:
   lin_value = 10^(dB_value/20);
*/
float logToLin (float imput)
{
 
  return(pow(10,(imput/20)));

}

// Main proccess Algorithm

static t_int *Dinamica_perform(t_int *w)
{
  // Object data
  t_Dinamica *x=(t_Dinamica *)(w[1]);  // Getting object
  t_float *p_in = (t_float *)(w[2]);  // Input signal buffer
  t_float *p_out = (t_float *)(w[3]);  // Output signal buffer
  int n= (int) (w[4]);  // Buffer Size
  
  // Auxiliar local processing variables
  float logImput=0;
  float approachedGain=0;
  float amplification=0;
  
  // Updating time coeff.
  if (x->AttacTime>=0)
  {

    x->AttacCoeff=timeCoefficient(x->AttacTime,x->sSR);
    x->AttacTime=-1;

  }
  if (x->ReleaseTime>=0)
  {

    x->p_ReleaseCoeff=timeCoefficient(x->ReleaseTime, x->sSR);
    x->ReleaseTime=-1;

  }
     
  while (n--)
  {

    // dBu imput sample level
    logImput=linToLog(*p_in);

    // Gain approaching
    approachedGain=0;
    if (logImput>x->Threshold)
    {
      approachedGain=gainComputer( logImput, x->Threshold, x->Ratio );
    }
    
    // Final gain calculation
    x->lastOutputGain=levelDetector(logImput, approachedGain,x->lastOutputGain, 
                                    x->AttacCoeff, x->p_ReleaseCoeff);
    
    // Amplification value
    amplification=logToLin(x->lastOutputGain);

    //Output value
    *out=amplification*(*p_in);

    // Output writing
    outlet_float(x->p_sOut, *p_out);
    
    p_in++;      
    p_out++;       
            
  }
  return (w+5);
}

// DSP tree builder
static void Dinamica_dsp(t_Dinamica *x,t_signal **sp)
{    
  // Adding routine to DSP tree
  dsp_add(Dinamica_perform,4,x, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);  

}

// Object Constructor
static void *Dinamica_new(void)
{

  t_Dinamica *x = (t_Dinamica *)pd_new(Dinamica_class);

  // Inlet allocation
  floatinlet_new(&x->x, &x->Threshold);
  floatinlet_new(&x->x, &x->Ratio);
  floatinlet_new(&x->x, &x->AttacTime);
  floatinlet_new(&x->x, &x->ReleaseTime);
  
  // Compression parameters initial set up
  x->Threshold=0;
  x->Ratio=2;
  x->ReleaseTime=10;
  x->AttacTime=10;
  x->sSR=44100;
  // Output allocation
  outlet_new(&x->x,gensym("signal"));
  x->p_sOut=outlet_new(&x->x,&s_float);

  return (x);

}

__declspec(dllexport) void Dinamica_tilde_setup(void);

// PD Object setup
void Dinamica_tilde_setup(void)
{

  Dinamica_class=class_new( gensym("Dinamica~"), (t_newmethod) Dinamica_new, 0, 
                            sizeof(t_Dinamica), 0, A_DEFFLOAT ,0 );
   
  class_addmethod(Dinamica_class,(t_method) Dinamica_dsp, gensym("dsp"),0);

  CLASS_MAINSIGNALIN(Dinamica_class,t_Dinamica,sIn);
     
}
